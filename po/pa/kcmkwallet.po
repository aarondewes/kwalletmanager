# translation of kcmkwallet.po to Punjabi
#
# Amanpreet Singh Brar <aalam@redhat.com>, 2005.
# A S Alam <aalam@users.sf.net>, 2007, 2010, 2014, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kcmkwallet\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-27 00:52+0000\n"
"PO-Revision-Date: 2019-02-10 08:26-0800\n"
"Last-Translator: A S Alam <alam.yellow@gmail.com>\n"
"Language-Team: Punjabi <punjabi-users@lists.sf.net>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "alam.yellow@gmail.com"

#: konfigurator.cpp:51 konfigurator.cpp:316 konfigurator.cpp:318
#: konfigurator.cpp:322 konfigurator.cpp:324
#, kde-format
msgid "KDE Wallet Control Module"
msgstr "KDE ਵਾਲਿਟ ਕੰਟਰੋਲ ਮੋਡੀਊਲ"

#: konfigurator.cpp:55
#, kde-format
msgid "(c) 2003 George Staikos"
msgstr "(c) 2003 ਜਾਰਜ ਸਟਾਕੋਸ"

#: konfigurator.cpp:56
#, kde-format
msgid "George Staikos"
msgstr "ਜਾਰਜ ਸਟਾਕੋਸ"

#: konfigurator.cpp:134 konfigurator.cpp:139
#, kde-format
msgid "New Wallet"
msgstr "ਨਵਾਂ ਵਾਲਿਟ"

#: konfigurator.cpp:135 konfigurator.cpp:140
#, kde-format
msgid "Please choose a name for the new wallet:"
msgstr "ਨਵੇਂ ਵਾਲਿਟ ਲਈ ਇੱਕ ਨਾਂ ਚੁਣੋ ਜੀ:"

#: konfigurator.cpp:273 konfigurator.cpp:363
#, kde-format
msgid "Always Allow"
msgstr "ਹਮੇਸ਼ਾ ਮਨਜ਼ੂਰ"

#: konfigurator.cpp:276 konfigurator.cpp:283 konfigurator.cpp:376
#, kde-format
msgid "Always Deny"
msgstr "ਹਮੇਸ਼ਾ ਪਾਬੰਦੀ"

#: konfigurator.cpp:316 konfigurator.cpp:322
#, kde-format
msgid "Permission denied."
msgstr "ਇਜਾਜ਼ਤ ਲਈ ਪਾਬੰਦੀ ਹੈ।"

#: konfigurator.cpp:318 konfigurator.cpp:324
#, kde-format
msgid ""
"Error while authenticating action:\n"
"%1"
msgstr ""
"ਕਾਰਵਾਈ ਪ੍ਰਮਾਣਿਤ ਕਰਨ ਦੌਰਾਨ ਗਲਤੀ:\n"
"%1"

#: konfigurator.cpp:427
#, kde-format
msgid ""
"This configuration module allows you to configure the KDE wallet system."
msgstr "ਇਹ ਸੰਰਚਨਾ ਮੋਡੀਊਲ ਤੁਹਾਨੂੰ KDE ਵਾਲਿਟ ਸਿਸਟਮ ਦੀ ਸੰਰਚਨਾ ਲਈ ਸਹਾਇਕ ਹੈ।"

#: konfigurator.cpp:442 konfigurator.cpp:444
#, kde-format
msgid "&Delete"
msgstr "ਹਟਾਓ(&D)"

#. i18n: ectx: attribute (title), widget (QWidget, tab1)
#: walletconfigwidget.ui:16
#, kde-format
msgid "Wallet Preferences"
msgstr "ਵਾਲਿਟ ਪਸੰਦ"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, _enabled)
#: walletconfigwidget.ui:22
#, kde-format
msgid ""
"<p>The wallet subsystem provides a convenient and secure way to manage all "
"your passwords. This option specifies whether or not you want to use it.</p>"
msgstr ""
"<p>ਵਾਲਿਟ ਸਬ-ਸਿਸਟਮ ਤੁਹਾਡੇ ਸਭ ਪਾਸਵਰਡ ਦੇ ਪਰਬੰਧ ਲਈ ਅਸਾਨ ਅਤੇ ਸੁਰੱਖਿਅਤ ਢੰਗ ਦਿੰਦਾ ਹੈ ਹੈ। ਤੁਸੀਂ "
"ਇਸ ਦੀ ਚੋਣ ਕਰ ਸਕਦੇ ਹੋ ਕਿ ਕੀ ਇਸ ਨੂੰ ਇਸ ਸਿਸਟਮ ਵਿੱਚ ਇਸ ਚੋਣ ਨਾਲ ਵਰਤਣਾ ਹੈ।</p>"

#. i18n: ectx: property (text), widget (QCheckBox, _enabled)
#: walletconfigwidget.ui:25
#, kde-format
msgid "&Enable the KDE wallet subsystem"
msgstr "KD&E ਵਾਲਿਟ ਸਬ-ਸਿਸਟਮ ਯੋਗ"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, groupBox2)
#: walletconfigwidget.ui:35
#, kde-format
msgid ""
"It is best to close your wallets when you are not using them to prevent "
"others from viewing or using them."
msgstr ""
"ਜਦੋਂ ਤੁਸੀਂ ਆਪਣੇ ਵਾਲਿਟ ਦੀ ਵਰਤੋਂ ਨਾ ਕਰ ਰਹੇ ਹੋਵੇਂ ਤਾਂ ਹੋਰਾਂ ਨੂੰ ਉਸ ਨੂੰ ਵੇਖਣ ਜਾਂ ਗਲਤ ਵਰਤੋਂ ਕਰਨ ਦੇਣ ਤੋਂ "
"ਰੋਕਣ ਲਈ ਇਸ ਨੂੰ ਬੰਦ ਕਰੋ ਦਿਓ।"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox2)
#: walletconfigwidget.ui:38
#, kde-format
msgid "Close Wallet"
msgstr "ਵਾਲਿਟ ਬੰਦ ਕਰੋ"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, _closeIdle)
#. i18n: ectx: property (whatsThis), widget (QSpinBox, _idleTime)
#: walletconfigwidget.ui:46 walletconfigwidget.ui:59
#, kde-format
msgid ""
"<p><b>Close wallet after a period of inactivity.</b><br>When a wallet is "
"closed the password is needed to access it again.</p>"
msgstr ""
"<p><b>ਕੁਝ ਦੇਰ ਦੀ ਗ਼ੈਰ-ਸਰਗਰਮੀ ਦੇ ਬਾਅਦ ਵਾਲਿਟ ਬੰਦ ਕਰ ਦਿਉ।</b><br>ਜਦੋਂ ਵਾਲਿਟ ਬੰਦ ਹੋ ਜਾਵੇਗਾ "
"ਤਾਂ ਇਸ ਨੂੰ ਵਰਤਣ ਲਈ ਫੇਰ ਪਾਸਵਰਡ ਦੀ ਲੋੜ ਪਵੇਗੀ।</p>"

#. i18n: ectx: property (text), widget (QCheckBox, _closeIdle)
#: walletconfigwidget.ui:49
#, kde-format
msgid "Close when unused for:"
msgstr "ਜਦੋਂ ਨਾ-ਵਰਤਿਆ ਜਾਵੇ ਤਾਂ ਬੰਦ:"

#. i18n: ectx: property (suffix), widget (QSpinBox, _idleTime)
#: walletconfigwidget.ui:62
#, fuzzy, kde-format
#| msgid " min"
msgid " minutes"
msgstr " ਮਿੰਟ"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, _screensaverLock)
#: walletconfigwidget.ui:99
#, kde-format
msgid ""
"<p><b>Close wallet as soon as the screensaver starts.</b><br>When a wallet "
"is closed the password is needed to access it again.</p>"
msgstr ""
"<p><b>ਜਿਵੇਂ ਹੀ ਸਕਰੀਨ-ਸੇਵਰ ਸ਼ੁਰੂ ਹੋਵੇ ਤਾਂ ਵਾਲਿਟ ਬੰਦ ਕਰ ਦਿਉ।</b><br>ਜਦੋਂ ਵਾਲਿਟ ਬੰਦ ਹੋ ਜਾਵੇਗਾ "
"ਤਾਂ ਇਸ ਨੂੰ ਵਰਤਣ ਲਈ ਫੇਰ ਪਾਸਵਰਡ ਦੀ ਲੋੜ ਪਵੇਗੀ।</p>"

#. i18n: ectx: property (text), widget (QCheckBox, _screensaverLock)
#: walletconfigwidget.ui:102
#, kde-format
msgid "Close when screensaver starts"
msgstr "ਜਦੋਂ ਸਕਰੀਨ-ਸੇਵਰ ਚਾਲੂ ਹੋਣ ਤਾਂ ਬੰਦ ਕਰੋ"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, _autoclose)
#: walletconfigwidget.ui:109
#, kde-format
msgid ""
"<p><b>Close wallet as soon as applications that use it have stopped.</"
"b><br>Note that your wallet will only be closed when all applications that "
"use it have stopped.<br>When a wallet is closed the password is needed to "
"access it again.</p>"
msgstr ""
"<p><b>ਜਿਵੇਂ ਹੀ ਐਪਲੀਕੇਸ਼ਨਾਂ ਵਾਲਿਟ ਨੂੰ ਵਰਤਣਾ ਬੰਦ ਕਰਨ ਤਾਂ ਵਾਲਿਟ ਬੰਦ ਕਰੋ।</b> <br>ਯਾਦ ਰੱਖੋ ਕਿ "
"ਤੁਹਾਡਾ ਵਾਲਿਟ ਤਾਂ ਹੀ ਬੰਦ ਹੋਵੇਗਾ, ਜਦੋਂ ਸਭ ਐਪਲੀਕੇਸ਼ਨ ਇਸ ਨੂੰ ਵਰਤਣਾ ਬੰਦ ਕਰ ਦੇਣਗੀਆਂ।<br>ਜਦੋਂ ਇਕਵਾਰ "
"ਵਾਲਿਟ ਬੰਦ ਹੋ ਗਿਆ ਤਾਂ ਇਸ ਨੂੰ ਵਰਤਣ ਲਈ ਫੇਰ ਪਾਸਵਰਡ ਦੀ ਲੋੜ ਪਵੇਗੀ।</p>"

#. i18n: ectx: property (text), widget (QCheckBox, _autoclose)
#: walletconfigwidget.ui:112
#, kde-format
msgid "Close when last application stops using it"
msgstr "ਜਦੋਂ ਆਖਰੀ ਐਪਲੀਕੇਸ਼ਨ ਨੇ ਵਰਤਣਾ ਬੰਦ ਕਰ ਦਿੱਤਾ ਹੋਵੇ ਤਾਂ ਬੰਦ ਕਰੋ"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox4)
#: walletconfigwidget.ui:122
#, kde-format
msgid "Automatic Wallet Selection"
msgstr "ਆਟੋਮੈਟਿਕ ਵਾਲਿਟ ਚੋਣ"

#. i18n: ectx: property (text), widget (QLabel, textLabel1_2)
#: walletconfigwidget.ui:128
#, kde-format
msgid "Select wallet to use as default:"
msgstr "ਡਿਫਾਲਟ ਰੂਪ ਵਿੱਚ ਵਰਤਣ ਲਈ ਵਾਲਿਟ ਚੁਣੋ:"

#. i18n: ectx: property (text), widget (QPushButton, _newWallet)
#. i18n: ectx: property (text), widget (QPushButton, _newLocalWallet)
#: walletconfigwidget.ui:161 walletconfigwidget.ui:191
#, kde-format
msgid "New..."
msgstr "ਨਵਾਂ..."

#. i18n: ectx: property (text), widget (QCheckBox, _localWalletSelected)
#: walletconfigwidget.ui:168
#, kde-format
msgid "Different wallet for local passwords:"
msgstr "ਲੋਕਲ ਪਾਸਵਰਡਾਂ ਲਈ ਵੱਖਰਾ ਵਾਲਿਟ:"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox3)
#: walletconfigwidget.ui:201
#, kde-format
msgid "Wallet Manager"
msgstr "ਵਾਲਿਟ ਮੈਨੇਜਰ"

#. i18n: ectx: property (text), widget (QCheckBox, _launchManager)
#: walletconfigwidget.ui:207
#, kde-format
msgid "Show manager in system tray"
msgstr "ਸਿਸਟਮ ਟਰੇ ਵਿੱਚ ਮੈਨੇਜਰ ਵੇਖੋ"

#. i18n: ectx: property (text), widget (QCheckBox, _autocloseManager)
#: walletconfigwidget.ui:235
#, kde-format
msgid "Hide system tray icon when last wallet closes"
msgstr "ਜਦੋਂ ਵਾਲਿਟ ਬੰਦ ਹੋਵੇ ਤਾਂ ਸਿਸਟਮ ਟਰੇ ਆਈਕਾਨ ਓਹਲੇ"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox5)
#: walletconfigwidget.ui:263
#, kde-format
msgid "Secret Service"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, _secretServiceAPI)
#: walletconfigwidget.ui:269
#, kde-format
msgid "Use KWallet for the Secret Service interface"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: walletconfigwidget.ui:279
#, kde-format
msgid ""
"The Secret Service interface lets applications store passwords and other "
"confidential data. Disable this to allow third-party wallet services such as "
"KeePassXC or GNOME Keyring to manage it instead."
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab2)
#: walletconfigwidget.ui:328
#, kde-format
msgid "Access Control"
msgstr "ਪਹੁੰਚ ਕੰਟਰੋਲ"

#. i18n: ectx: property (text), widget (QCheckBox, _openPrompt)
#: walletconfigwidget.ui:334
#, kde-format
msgid "&Prompt when an application accesses a wallet"
msgstr "ਜਦੋਂ ਇੱਕ ਐਪਲੀਕੇਸ਼ਨ ਇੱਕ ਵਾਲਿਟ ਨੂੰ ਵਰਤਣ ਦੀ ਕੋਸ਼ਿਸ਼ ਕਰੇ ਤਾਂ ਪੁੱਛੋ(&P)"

#. i18n: ectx: property (text), widget (QTreeWidget, _accessList)
#: walletconfigwidget.ui:348
#, kde-format
msgid "Wallet"
msgstr "ਵਾਲਿਟ"

#. i18n: ectx: property (text), widget (QTreeWidget, _accessList)
#: walletconfigwidget.ui:359
#, kde-format
msgid "Application"
msgstr "ਐਪਲੀਕੇਸ਼ਨ"

#. i18n: ectx: property (text), widget (QTreeWidget, _accessList)
#: walletconfigwidget.ui:370
#, kde-format
msgid "Policy"
msgstr "ਪਾਲਸੀ"

#. i18n: ectx: property (text), widget (QPushButton, _launch)
#: walletconfigwidget.ui:406
#, kde-format
msgid "&Launch Wallet Manager"
msgstr "ਵਾਲਿਟ ਮੈਨੇਜਰ ਚਲਾਓ(&L)"

#~ msgid "kcmkwallet"
#~ msgstr "kcmkwallet"

#~ msgid ""
#~ "Sorry, the system security policy didn't allow you to save the changes."
#~ msgstr "ਅਫਸੋਸ, ਸਿਸਟਮ ਸੁਰੱਖਿਆ ਪਾਲਸੀ ਤੁਹਾਨੂੰ ਬਦਲਾਅ ਸੰਭਾਲਣ ਦੀ ਮਨਜ਼ੂਰੀ ਨਹੀਂ ਦਿੰਦੀ ਹੈ।"
